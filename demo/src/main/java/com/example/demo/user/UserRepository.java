package com.example.demo.user;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import jakarta.persistence.LockModeType;

import com.example.demo.user.User;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, Integer> {

    Iterable<User> findByName(String name);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Iterable<User> findForUpdateByName(String name);
}