package com.example.demo.host;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/host") // This means URL's start with /demo (after Application path)
public class HostController {
    @GetMapping(path = { "/", "" })
    public @ResponseBody String getHostName() throws UnknownHostException {
        // This returns a JSON or XML with the users
        InetAddress addr = InetAddress.getLocalHost();
        return "server hostname is: " + addr.getHostName();
    }
}
