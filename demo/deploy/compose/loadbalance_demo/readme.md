# 部署服务


## 完成 ecs-no-db 试验后，进行服务的 负载均衡配置


### 启动编排的服务
```bash

mkdir -p loadbalance_demo/
cd loadbalance_demo


# 在 本地电脑 创建 容器编排 文件， 
# 修改 数据库 地址 为 内网地址
# 修改 数据库账户密码
# 内容参考 demo/deploy/loadbalance_demo/docker-compose.yml
# demo/deploy/loadbalance_demo/nginx.conf
# 本地准备好 loadbalance_demo 目录，把 loadbalance_demo/docker-compose.yml 中的账号密码更换成自己的

# 远程同步目录
scp -r c:/xxxxx/loadbalance_demo  root@服务器ip:/root/

# 必须在 docker-compose.yml 文件 所在目录下 执行
cd /root/loadbalance_demo/

# 检查服务器上 nginx.conf 和 docker-compose.yml 是否存在
ls 

# 更新镜像
docker pull  registry.cn-hangzhou.aliyuncs.com/mkmk/java:demo

docker compose up -d

```


# 在ecs 中测试服务

```bash

curl http://localhost:8080/host; echo
# server hostname is: 51e7a2261da8

curl http://localhost:8080/host; echo
# server hostname is: 751f0518d9fb

curl http://localhost:8080/user/all
# [{"id":1,"name":"xiaoming","email":"test@example.com","age":0}]

```

我们访问 同一个 nginx 请求 就会被 代理给 spring 的不同主机
他们除了 host 接口返回的 主机名不同外

他们 提供的 用户查询， 创建用户的数据 都是 共享的 同一个 数据库

同时 数据库 的 事务 的 读写锁lock， 可以 让多个实例之间 保证 业务数据的一致性


# 在浏览器访问服务

登录 ecs 打开 安全组， 放行 8080 端口 源：0.0.0.0/0

http://<ecs公网ip>:8080/host

# 关闭 compose 服务
```bash

cd  yml文件所在目录

docker compose down

```
