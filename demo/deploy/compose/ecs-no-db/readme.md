# 部署服务


## ecs-no-db, 使用外部数据库


### 创建 ecs 云主机
安装 docker

### 创建 rds mysql 数据库

新用户登录 
https://free.aliyun.com
免费创建  rds mysql 

1. 必须和 ecs 位于 相同地域

2. 控制台搜索 rds 服务

3. 创建数据库 springdemo

4. 创建账户密码  

5. 将 ecs 添加到 rds 的白名单


### 启动编排的服务
```bash
mkdir -p compose/
cd compose

# 创建 容器编排 文件， 
# 修改 数据库 地址 为 内网地址
# 修改 数据库账户密码
# 内容参考 demo/deploy/compose/ecs-no-db/docker-compose.yml
vi docker-compose.yml

# 除了 vim 编辑文件， 也可以 本地编写好 文件 发送到 服务器
scp c:/xxxxx/docker-compose.yml   root@服务器ip:/root/compose/


# 创建完 docker-compose.yml 后，必须在 yml 文件 所在目录下 执行
docker compose up -d

```


# 在ecs 中测试服务

```bash
curl http://localhost:8080/hello

curl -X POST -d 'name=xiaoming' -d 'email=test@example.com' http://localhost:8080/user/add
# Saved

curl http://localhost:8080/user/all
# [{"id":1,"name":"1","email":"1"},{"id":2,"name":"xiaoming","email":"tes@example.com"}]

```

# 在浏览器访问服务

登录 ecs 打开 安全组， 放行 8080 端口 源：0.0.0.0/0

http://<ecs公网ip>:8080/user/all

# 关闭 compose 服务
```bash

cd  yml文件所在目录

docker compose down

```
