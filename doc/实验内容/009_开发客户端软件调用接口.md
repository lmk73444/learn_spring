
# 客户端开发
拓展:
我们之前的 课程 已经 讲过 服务器 开发 入门，服务器 一般是 部署在 云上的
应用程序 和 小程序 都可以 连接 云服务器
应用 客户端 不要求 https 域名访问
小程序 必须要 申请 ssl 证书，才能 上线

选择 一个 界面开发教程， 调用 下 之前 部署的 用户创建 和 查询接口， 做一个 小软件 出来

# maui 应用客户端 （开发 windows 和 安卓应用程序）
https://dotnet.microsoft.com/zh-cn/learn/maui/first-app-tutorial/intro

##  maui 网络模块 连接 服务器
https://learn.microsoft.com/zh-cn/dotnet/maui/platform-integration/communication/networking?tabs=windows


# 小程序开发
https://developers.weixin.qq.com/miniprogram/dev/framework

## 小程序 网络模块 连接 服务器
https://developers.weixin.qq.com/miniprogram/dev/framework/ability/network.html

