# 创建步骤
https://free.aliyun.com/

产品类型 
数据库
关系型数据库

创建 rds mysql 数据库

# 文档
https://help.aliyun.com/zh/rds/apsaradb-rds-for-mysql/create-an-apsaradb-rds-for-mysql-instance?spm=a2c4g.11186623.0.i12#concept-wzp-ncf-vdb

# 学习内容

掌握 创建 用户
掌握 创建 数据库
设置 数据库 白名单
会查找 数据据库 内网地址
