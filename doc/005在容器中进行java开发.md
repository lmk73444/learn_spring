
# 容器改名 （可以跳过）
因为大家网络 访问 docker 官网 太慢， 所以先把 镜像源 改成 
阿里 容器镜像服务  acr

```bash

docker pull bellsoft/liberica-openjdk-debian:17

docker tag bellsoft/liberica-openjdk-debian:17  registry.cn-hangzhou.aliyuncs.com/mkmk/java:17

docker push  registry.cn-hangzhou.aliyuncs.com/mkmk/java:17


```

# 创建 开发环境，在容器中编译 java

```bash

# --rm 不保存，推出就销毁  
# bash 容器启动的进程
docker run -it --rm registry.cn-hangzhou.aliyuncs.com/mkmk/java:17  bash

cat > HelloJava.java <<EOF
class HelloJava{
	public static void main(String[] args){
		System.out.println("Hello World!");
	}
}
EOF

# 编译 java class
javac HelloJava.java

ls *.class
HelloJava.class

java HelloJava
# Hello World!

```