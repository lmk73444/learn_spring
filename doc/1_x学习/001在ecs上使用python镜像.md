# 使用 python 镜像
在镜像 仓库 查找 合适的 python镜像
https://cr.console.aliyun.com/cn-hongkong/instances/artifact

基础镜像 -> 阿里云 -> linux -> x86-64

alibaba-cloud-linux-3-registry.cn-hangzhou.cr.aliyuncs.com/alinux3/python:3.11.1

# 启动镜像

在 docker 主机 上执行
```bash

# --rm 会在使用后 销毁容器， 节约空间
# 为了 将代码 保留在服务器 可以 使用 -v 将工作目录 持久化
# 在 host 上执行
mkdir -p /my_code

docker run -it --rm -v /my_code:/my_code -w /my_code alibaba-cloud-linux-3-registry.cn-hangzhou.cr.aliyuncs.com/alinux3/python:3.11.1  bash

pwd
# /my_code

# 在容器中
cat > test.py <<"EOF"

def hello():
  print("hello world")

hello()
EOF

python3 test.py
# hello world

```

# 总结

当后续我们 学习 其他 语言 或者 linux 常见 软件的 时候， 
也可以 直接 在 镜像 仓库， 查找 合适的 镜像
直接使用， 避免了 安装软件的麻烦

