import socket

#创建一个socket对象，指定要连接的目标服务器 ip及端口号
# 第一步
s =  socket.socket()
# 第二步
s.connect(('socket-server',9999))

while True:
    recv_data = str(s.recv(20480), encoding='utf8') #客户端接收来自服务器端发送的数据
    print(recv_data)
    #连接成功后向服务器端发送数据
    while True:
        send_data = input('').strip()
        if send_data  in ["0", "1", "2"]:
            break
        print("输入错误: 重新输入， 0， 1， 2")
        
    s.sendall(bytes(send_data,encoding = 'utf8'))
    if send_data=='bye':
           break

s.close()

