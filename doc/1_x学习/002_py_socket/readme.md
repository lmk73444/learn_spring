# python socket试验

```bash

mkdir /root/git_proj
cd /root/git_proj

# 首次 clone 项目
git clone https://gitee.com/lmk73444/learn_spring.git

# 非首次
# 更新项目
cd /root/git_proj/learn_spring
git remote update
git checkout -f origin/master

# 进入实验目录
cd /root/git_proj/learn_spring/doc/1_x学习/002_py_socket

# 在宿主机 启动 猜数服务器
docker compose up -d
# [+] Running 3/3
#  ✔ Network 002_py_socket_default            Created                                                                0.1s
#  ✔ Container 002_py_socket-socket-client-1  Started                                                                0.1s
#  ✔ Container 002_py_socket-socket-server-1  Started


# 打印客户端输出
docker logs  002_py_socket-socket-client-1
# 请出拳:(0) 石头(1) 剪刀(2) 布

# 绑定 client 容器输入输出
docker attach  002_py_socket-socket-client-1
# 0
# 你出了：石头,计算机出了：石头平局请出拳:(0) 石头(1) 剪刀(2) 布
# 1
# 你出了：剪刀,计算机出了：布你赢了！请出拳:(0) 石头(1) 剪刀(2) 布
# 1

# 如果出现 
# You cannot attach to a stopped container, start it first
# 重新 启动 客户端
docker compose up -d


```

# 尝试着 为游戏 添加 逻辑

## 将提示 修改为 请你出拳
```bash
# ctrl + d 退出游戏

cd /root/git_proj/learn_spring/doc/1_x学习/002_py_socket
vi server.py

# i 进入编辑模式， 修改 游戏提醒
# esc :wq 保存文件

# 重启游戏
docker compose restart 

# 验证修改
docker attach  002_py_socket-socket-client-1

```

## 尝试为 游戏 添加 计算机 获胜次数， 以及 玩家 获胜 次数

修改 server.py
