# 本地调试代码

打开需要调试的代码， main 函数文件，点击 F5， 开始调试

在想要 debug 的代码左侧，打上断点，访问对应的url 触发断点，即可逐步调试代码

再次f5 ，从断点处继续执行
f10 逐行执行
f11 跟随函数调用，逐行执行

localhost:8081/hello

localhost:8081/user/add

localhost:8081/user/all
