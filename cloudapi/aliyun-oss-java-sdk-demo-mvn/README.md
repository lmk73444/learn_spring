# OSS Java SDK Demo

## 关于
maven-demo是OSS Java SDK的应用示例，基于maven的工程。

## 编译运行
编译运行前请修改HelloOSS.java中endpoint、accessKeyId、accessKeySecret、bucketName为您的真实信息。

### 编译
在工程目录下执行`mvn clean package`。

如果找不到 mvn
```bash

/root/.m2/wrapper/dists/apache-maven-3.9.4-bin/32a55694/apache-maven-3.9.4/bin/mvn  clean package

```

### 运行
在工程目录下执行`java -jar target/maven-demo-0.1.1.jar`。
